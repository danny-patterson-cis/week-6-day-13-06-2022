import './App.css';
import Parent1 from './Components/ChildToParent/Parent1';
import Parent from './Components/ParentToChild/Parent';

function App() {
  return (
    <div className="App">
      <div>
        Parent To Child
        <Parent></Parent>
      </div>
      <div>
        Child To Parent
        <Parent1 />
      </div>
    </div>
  );
}

export default App;
