import React from 'react'
import Child1 from './Child1'
import { useState } from 'react';

const Parent1 = () => {
    const [data, setData] = useState('');
    const [count, setCount] = useState(0)
    const [value, setValue] = useState(0)
    const [name, setName] = useState('')

    const childToParent = (childData) => {
        setData(childData);
        setCount(count + 5)
        setName(value)
    }

    const onChangeHandler = (e) => {
        setValue(e.target.value)
    }
    return (
        <div>
            <Child1 childToParent={childToParent} onChange={onChangeHandler}></Child1>
            <div>{data}</div>
            <div>{count}</div>
            <div>Name:{name}</div>
        </div>
    )
}

export default Parent1