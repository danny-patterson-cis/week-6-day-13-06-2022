import React from 'react'
import Child from './Child'
import { useState } from 'react';

const Parent = () => {
    console.log("parent render")
    const [data, setData] = useState('');
    const [count, setCount] = useState(0);
    const parentToChild = () => {
        setData("This is data from Parent Component to the Child Component.");
        setCount(count + 1);
    }
    const minusCount = () => {
        setCount(count - 1);
    }

    return (
        <div>
            <Child data={data} count={count} minusCount={minusCount} />
            <div className="d-grid gap-2 d-md-block">
                <button className="btn btn-primary" type="button" onClick={() => parentToChild()}>parentToChild</button>

            </div>
        </div>
    )
}

export default Parent