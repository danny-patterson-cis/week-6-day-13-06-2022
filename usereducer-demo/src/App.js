import React, { useReducer } from "react";

const initialState = 0;
const reducer = (state, action) => {
  switch (action) {
    case "add":
      return state + 1;
    case "subtract":
      return state - 1;
    case "reset":
      return 0;
    default:
      throw new Error("Unexpected action");
  }
};

function App() {
  const [count, dispatch] = useReducer(reducer, initialState);
  return (
    <div className="App">
      <h2>{count}</h2>
      <div className="d-grid gap-2 d-md-block">
        <button className="btn btn-primary" onClick={() => dispatch("add")}>Add</button>
        <button className="btn btn-danger" onClick={() => dispatch("subtract")}>subtract</button>
        <button className="btn btn-success" onClick={() => dispatch("reset")}>Reset</button>
      </div>
    </div>
  );
}

export default App;
